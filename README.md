
MAX7219CurrentTester

  MAX7219CurrentTester is an ATMEGA328P firmware for fully illuminating a MAX7219/MAX7221 chip's LED array, so that a multimeter can be used to measure the total current consumption.
  It should work on Arduinos too.
  
  Pin assignments:
    - MAX72\*\*: 1  (DIN)  <-> ATMEGA328P: 11 (PD5)
    - MAX72\*\*: 12 (LOAD) <-> ATMEGA328P: 12 (PD6)
    - MAX72\*\*: 13 (CLK)  <-> ATMEGA328P: 13 (PD7)
  
  How to use this:
    1. Build a standard 5v-regulator-based power circuit. I use a 7805 regulator and two 10uF electrolytic capacitors in the standard configuration, fed by an AC/DC adapter plugged into a barrel jack. (Observe all relevant electrical safety precautions here!)
    2. Build a standard standalone ATMEGA328P circuit.
    3. Build a standard MAX72\*\* LED-matrix circuit.
    4. Disconnect all connections from +5v to the MAX72\*\* circuit.
    5. Connect +5v to your multimeter's positive lead.
    6. Connect your multimeter's negative lead to all +5v connections in the MAX72\*\*'s circuit.
    7. Connect the ATMEGA328P and MAX72\*\* circuits together with the aforementioned pin-assignments.
    8. Connect your AVR-ISP programmer (or equivalent device) to the ATMEGA's programming pins, and then use "Upload With Programmer" in the Arduino IDE to upload the MAX7219CurrentTester firmware to the ATMEGA328P chip.
    9. When swapping out one RSET resistor for another, make sure the circuit is completely turned off. To do otherwise would instantly destroy the MAX72\*\* chip.
