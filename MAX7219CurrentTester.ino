


/*

		MAX7219CurrentTester is an ATMEGA328P firmware for testing the current consumed by a MAX7219/MAX7221 chip and its LED array.
    It should work on Arduinos too.
    
		Copyright (C) 2019, C.D.M. RÃ¸rmose (sevenplagues@gmail.com).

		This program is free software; you can redistribute it and/or modify
		it under the terms of the GNU General Public License as published by
		the Free Software Foundation; either version 2 of the License, or
		(at your option) any later version.

		This program is distributed in the hope that it will be useful,
		but WITHOUT ANY WARRANTY; without even the implied warranty of
		MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	See the
		GNU General Public License for more details.

		You should have received a copy of the GNU General Public License along
		with this program; if not, write to the Free Software Foundation, Inc.,
		51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

*/



// Serial Peripheral Interface library
#include <SPI.h>



// Initialize communications with the MAX72** chip,
// on regular digital pins.
void maxInitialize() {

	DDRD |= B11100000; // Set the Data, Clk, and CS pins as outputs

	sendMaxCmd(15, 0); // Conduct a display-test
	sendMaxCmd(11, 7); // Set scanlimit to max
	sendMaxCmd(9, 0); // Decode is done in source

	// Clear all rows
	sendRow(0, 0);
	sendRow(1, 0);
	sendRow(2, 0);
	sendRow(3, 0);
	sendRow(4, 0);
	sendRow(5, 0);
	sendRow(6, 0);
	sendRow(7, 0);

	sendMaxCmd(12, 0); // Shutown mode: shutdown
	sendMaxCmd(12, 1); // Shutown mode: startup
	sendMaxCmd(10, 15); // LED intensity: full brightness (range is 0 to 15)

}

// Update the LED-data of a given row on the MAX72** chip
void sendRow(volatile byte row, volatile byte d) {
	sendMaxCmd(row + 1, d); // Send the row's opcode and data byte
}

// Send a command to the MAX72** chip
void sendMaxCmd(volatile byte cmd, volatile byte d) {
	PORTD &= B10111111; // Set the CS pin low (data latch)
	shiftOut(5, 7, MSBFIRST, cmd); // Send the command's opcode
	shiftOut(5, 7, MSBFIRST, d); // Send the command's 1-byte LED-data payload
	PORTD |= B01000000; // Set the CS pin high (data latch)
}



void setup() {

  maxInitialize(); // Initialize the MAX72** chip's LED system

  Serial.begin(31250); // Start serial comms (we don't use these, but might as well be thorough)
  
  for (byte i = 0; i < 8; i++) { // For every LED-row, illuminate all the row's LEDs
    sendRow(i, 255);
  }

}


// Empty main loop, because all testing commands are sent on setup
void loop() {
}

